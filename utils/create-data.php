<?php

const SKILL_EASY = 0;
const SKILL_AVERAGE = -1;
const SKILL_HARD = -2;
const SKILL_VERY_HARD = -3;

$id = 99999;
$difficulties = [
    'EASY' => SKILL_EASY,
    'AVERAGE' => SKILL_AVERAGE,
    'HARD' => SKILL_HARD,
    'VERY HARD' => SKILL_VERY_HARD,
    'E' => SKILL_EASY,
    'A' => SKILL_AVERAGE,
    'H' => SKILL_HARD,
    'VH' => SKILL_VERY_HARD,
];

function create($file, $data) {
    global $id, $difficulties;

    if (file_exists($file)) {
        unlink($file);
    }

    $handle = fopen($file, 'w');
    foreach ($data as $row) {
        if (isset($row['data']['difficulty'])) {
            $diff = strtoupper($row['data']['difficulty']);
            if (isset($difficulties[$diff])) {
                $row['data']['difficulty'] = $difficulties[$diff];
            }
        }

        $id ++;
        $row['_id'] = str_pad(base_convert($id, 10, 36), 8, 'a', STR_PAD_LEFT);
        $row['flags'] = new stdClass();
        $row['img'] = 'icons/svg/mystery-man.svg';
        $row['permission'] = ['default' => 0];
        $row['effects'] = [];
        fwrite($handle, json_encode($row)."\n");
    }
    fclose($handle);
}

function minify($file) {
    $content = json_decode(file_get_contents($file), true);
    file_put_contents($file, json_encode($content));
}

function unminify($file) {
    $content = json_decode(file_get_contents($file), true);
    $handle = fopen($file, 'w');
    fwrite($handle, "[\n");

    foreach ($content as $index => $row) {
        fwrite($handle, "\t".json_encode($row));
        if ($index < count($content) - 1) {
            fwrite($handle, ",");
        }
        fwrite($handle, "\n");
    }
    fwrite($handle, "]");
}

function updateSkillsSchema($file) {
    $data = json_decode(file_get_contents($file), true);
    $updated = [];

    foreach ($data as $row) {
        if (!isset($row['data']['specialisation'])) {
            $row['data']['specialisation'] = '';
        }
        $updated[] = $row;
    }
    file_put_contents($file, json_encode($updated));
}

function updateAdvantagesSchema($file) {
    $data = json_decode(file_get_contents($file), true);
    $updated = [];

    foreach ($data as $row) {
        if (!isset($row['data']['value'])) {
            $row['data']['value'] = 1;
        }
        if (!isset($row['data']['description'])) {
            $row['data']['description'] = '';
        }
        $updated[] = $row;
    }
    file_put_contents($file, json_encode($updated));
}

$advantages = glob(__DIR__.'/data/advantages/*.json');
$skills = glob(__DIR__.'/data/skills/*.json');
$opt = getopt('', [
    'minify',
    'unminify',
    'update',
]);

if (isset($opt['minify'])) {
    foreach(array_merge($advantages, $skills) as $file) {
        minify($file);
    }
} elseif (isset($opt['unminify'])) {
    foreach(array_merge($advantages, $skills) as $file) {
        unminify($file);
    }
} elseif(isset($opt['update'])) {
    foreach ($advantages as $file) {
        updateAdvantagesSchema($file);
    }
    foreach ($skills as $file) {
        updateSkillsSchema($file);
    }
} else {
    foreach($advantages as $file) {
        $name = substr(basename($file), 0, -5);
        create(dirname(__DIR__)."/packs/advantages-{$name}.db", json_decode(file_get_contents($file), true));
    }

    foreach($skills as $file) {
        $name = substr(basename($file), 0, -5);
        create(dirname(__DIR__)."/packs/skills-{$name}.db", json_decode(file_get_contents($file), true));
    }
}

