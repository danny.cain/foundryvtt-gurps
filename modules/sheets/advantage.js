export default class GurpsAdvantageSheet extends ItemSheet {
    constructor(...args) {
        super(...args);

        // extends any constructor stuff here
    }

    /** @override */
    static get defaultOptions() {
        return mergeObject(super.defaultOptions, {

        });
    }

    /** @override */
    get template() {
        return `systems/foundryvtt-gurps/templates/items/advantage-sheet.html`
    }

    /** @override */
    getData() {
        // get data for character sheet
        // each key from this object is available aas a root level object in the template
        const data = {
            type: this.item.data.type,
            item: duplicate(this.item.data),
        };

        return data;
    }

    activateListeners(html) {
        // editable only listeners
        if (this.isEditable) {
            const inputs = html.find('input');
            inputs.focus(ev => ev.currentTarget.select());
        }

        // owner only listeners
        if (this.item.owner) {

        } else {
            // remove rollable classes
            html.find(".rollable").each((i, el) => el.classList.remove("rollable"));
        }

        super.activateListeners(html);
    }
}