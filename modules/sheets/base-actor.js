export default class GurpsActorSheet extends ActorSheet {
    constructor(...args) {
        super(...args);

        // extends any constructor stuff here
    }

    /** @override */
    static get defaultOptions() {
        return mergeObject(super.defaultOptions, {
            scrollY: [
                ".inventory .inventory-list"
            ],
            tabs: [{ navSelector: ".tabs", contentSelector: ".sheet-body", initial: "description" }],
        });
    }

    /** @override */
    get template() {
        if (!game.user.isGM && this.actor.limited) return "systems/foundryvtt-gurps/templates/actors/limited.html";
        return `systems/foundryvtt-gurps/templates/actors/${this.actor.data.type}-sheet.html`
    }

    /** @override */
    getData() {
        // get data for character sheet
        // each key from this object is available aas a root level object in the template
        const data = {
            type: this.actor.data.type,
            actor: duplicate(this.actor.data),
            skills: this.getSkills(),
            advantages: this.getAdvantages(),
            equipment: this.getEquipment(),
        };

        return data;
    }

    getSkills(name = '') {
        const numColumns = 2;
        const skills = [...this.actor.data.items.filter((item) => item.type === 'skill' && (item.name.indexOf(name) > -1)).map((skill) => {
            const characteristic = this.actor.data.data[skill.data.characteristic].value;
            const difficulty = parseInt(skill.data.difficulty) - 1;
            const value = parseInt(skill.data.value);

            skill.modifier = difficulty + value;
            skill.total = characteristic + difficulty + value;

            return skill;
        }).sort((a, b) => {
            return b.name > a.name ? -1 : 1;
        })];

        let columns = [];
        let columnSize = Math.ceil(skills.length / numColumns);
        for (let i = 0 ; i < numColumns; i ++) {
            columns.push(skills.splice(0, columnSize));
        }

        return columns;
    }

    getAdvantages() {
        return this.actor.data.items.filter((item) => item.type === 'advantage').map((advantage) => {
            advantage.icons = [];
            if (advantage.data.type === 'physical') {
                advantage.icons.push('/systems/foundryvtt-gurps/assets/physical.png');
            } else if (advantage.data.type === 'mental') {
                advantage.icons.push('/systems/foundryvtt-gurps/assets/mental.png');
            } else if (advantage.data.type === 'social') {
                advantage.icons.push('/systems/foundryvtt-gurps/assets/social.png');
            }

            if (advantage.data.category === 'exotic') {
                advantage.icons.push('/systems/foundryvtt-gurps/assets/exotic.png');
            } else if (advantage.data.category === 'supernatural') {
                advantage.icons.push('/systems/foundryvtt-gurps/assets/supernatural.png');
            }
            return advantage;
        }).sort((a, b) => {
            return b.name > a.name ? -1 : 1;
        });
    }

    getEquipment() {
        return this.actor.data.items.filter((item) => item.type === 'equipment').sort((a, b) => {
            return b.name > a.name ? -1 : 1;
        });
    }

    say(text) {
        CONFIG.ChatMessage.entityClass.create({
            user: game.user._id,
            speaker: ChatMessage.getSpeaker({ actor: this.actor }),
            type: CONST.CHAT_MESSAGE_TYPES.OOC,
            content: text,
            sound: CONFIG.sounds.notification,
        });
    }

    calculateTarget(attribute) {
        let attr = null;
        let skill = this.getSkills(attribute).shift().shift();

        if (this.actor.data.data[attribute]) {
            attr = this.actor.data.data[attribute];
        } else if (skill) {
            attr = skill.data;
        }

        if (!attr) {
            console.error(skill);
            this.say(`Unrecognised attribute '${attribute}`);
            return null;
        }

        if (attr.characteristic) {
            return this.calculateTarget(attr.characteristic) + attr.value + parseInt(attr.difficulty) - 1;
        }
        return attr.value;
    }

    isCrit(effectiveScore, rollTotal) {
        if (effectiveScore >= 16 && rollTotal <= 6) {
            return true;
        } else if (effectiveScore >= 15 && rollTotal <= 5) {
            return true;
        } else if (rollTotal <= 4) {
            return true;
        }

        if (effectiveScore <= 15 && rollTotal >= 17) {
            return true;
        } else if (rollTotal - effectiveScore >= 10) {
            return true;
        } else if (rollTotal === 18) {
            return true;
        }
        return false;
    }

    _roll($rollable, prompt = false) {
        const stat = $rollable.data('roll');
        const roll = new Roll('3d6', this.actor.getRollData());
        const result = roll.roll();
        let target = this.calculateTarget(stat);

        if (!target) {
            return;
        }

        let modifier = 0;
        if (prompt) {
            modifier = parseInt(window.prompt('Enter modifier'));
        }
        target += modifier;

        const total = result.results[0];
        const critical = this.isCrit(target, total);

        let effect = target - total;
        if (total <= 4 && effect < 0) {
            effect = 0;
        }
        if (total >= 17 && effect >= 0) {
            effect = -1;
        }

        
        if (modifier >= 0) {
            modifier = '+' + modifier;
        }
        let rollDescription = `<h3>Rolling ${stat} ${modifier} (${target}): ${total}</h3>`;
        let critText = '';
        const success = effect >= 0;
        const color = success ? 'green' : 'red';
        if (critical && effect >= 0) {
            critText = 'CRIT Success';
        } else if (critical && effect < 0) {
            critText = 'CRIT Failure';
        }
        if (success) {
            effect = '+' + effect;
        }
        effect = `(<span style="font-weight:bold; color: ${color};">${effect} ${critText}</span>)`;

        CONFIG.ChatMessage.entityClass.create({
            user: game.user._id,
            speaker: ChatMessage.getSpeaker({ actor: this.actor }),
            type: CONST.CHAT_MESSAGE_TYPES.ROLL,
            flavor: `
                    ${rollDescription} ${effect}
                    `,
            sound: CONFIG.sounds.dice,
            roll: result,
        });
    }

    activateListeners($doc) {
        // editable only listeners
        let sheet = this;
        let itemTemplates = {
            advantage: {

            },
            equipment : {
                cost: 0,
                weight: 0,
                description: ""
            },
            skill: {

            }
        };

        if (this.isEditable) {
            const inputs = $doc.find('input');
            inputs.focus(ev => ev.currentTarget.select());

            $doc.find('[data-action="edit"]').click(function() {
                const itemId = $(this).data('item');
                sheet.actor.getOwnedItem(itemId).sheet.render(true);
            });

            $doc.find('[data-action="destroy"]').click(function() {
                const itemId = $(this).data('item');

                Dialog.confirm({
                    title: 'Are you sure?',
                    content: '<p>Are you sure you wish to delete this?</p>',
                    yes() {
                        sheet.actor.getOwnedItem(itemId).delete();
                    }
                });
            });

            $doc.find('[data-action="create"]').click(async function() {
                const type = $(this).data('type');
                let item = {
                    name: `New ${type}`,
                    type: type,
                    data: { ...(itemTemplates[type] || {}) }
                };

                let instance = await sheet.actor.createEmbeddedEntity("OwnedItem", item);
                sheet.actor.getOwnedItem(instance._id).sheet.render(true);
            });
        }

        // owner only listeners
        if (this.actor.owner) {
            $doc.find('.rollable').click(function(evt) {
                sheet._roll($(this), evt.shiftKey);
            });

        } else {
            // remove rollable classes
            $doc.find(".rollable").each((i, el) => el.classList.remove("rollable"));
        }

        $doc.find('[data-action="debug"]').on('click', () => {
            console.log(this.actor.data);
            this.say(JSON.stringify(this.actor.data.data));
        });

        super.activateListeners($doc);
    }

    /** @override */
    _updateObject(evt, form) {
        // todo - can update any dynamic properties here and clamp values if needed
        return this.object.update(form);
    }
}