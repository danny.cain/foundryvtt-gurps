export default class GurpsItemSheet extends ItemSheet {
    constructor(...args) {
        super(...args);

        // extends any constructor stuff here
    }

    /** @override */
    static get defaultOptions() {
        return mergeObject(super.defaultOptions, {

        });
    }

    /** @override */
    get template() {
        const type = 'base' && this.item && this.item.data && this.item.data.type;
        return `systems/foundryvtt-gurps/templates/items/${type}-sheet.html`
    }

    /** @override */
    getData() {
        // get data for character sheet
        // each key from this object is available aas a root level object in the template
        const data = {
            type: this.item.data.type,
            item: duplicate(this.item.data),
        };
        console.log(this.item);
        return data;
    }

    say(text) {
        CONFIG.ChatMessage.entityClass.create({
            user: game.user._id,
            speaker: ChatMessage.getSpeaker({ actor: this.actor }),
            type: CONST.CHAT_MESSAGE_TYPES.OOC,
            content: text,
            sound: CONFIG.sounds.notification,
        });
    }

    activateListeners($doc) {
        // editable only listeners
        if (this.isEditable) {
            const inputs = $doc.find('input');
            inputs.focus(ev => ev.currentTarget.select());
        }

        $doc.find('[data-action="debug"]').on('click', () => {
            console.log(this.item.data);
            this.say(JSON.stringify(this.item.data.data));
        });

        // owner only listeners
        if (this.item.owner) {

        } else {
            // remove rollable classes
            $doc.find(".rollable").each((i, el) => el.classList.remove("rollable"));
        }

        super.activateListeners($doc);
    }
}