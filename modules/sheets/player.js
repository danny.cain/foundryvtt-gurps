import BaseActor from './base-actor.js';

export default class GurpsPlayerSheet extends BaseActor {
    /** @override */
    static get defaultOptions() {
        return mergeObject(super.defaultOptions, {
            dragDrop: [
                { dragSelector: '.item-list .item', dropSelector: null }
            ],
        });
    }
}