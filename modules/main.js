import GurpsActor from "./entities/actor.js";
import GurpsItem from "./entities/item.js";
import GurpsPlayerSheet from "./sheets/player.js";
import GurpsItemSheet from "./sheets/item.js";

Hooks.once('init', () => {
    console.log('Gurps - Initialising');

    // setup any global scope you need
    game.gurps = {

    };

    CONFIG.Actor.entityClass = GurpsActor;
    CONFIG.Item.entityClass = GurpsItem;
    CONFIG.time.roundTime = 6;

    // registerSystemSettings();

    // todo - configure initiative
    // CONFIG.Combat.initiative.formula = "1d20 + @attributes.init.mod";

    // unregister default sheets
    Actors.unregisterSheet("core", ActorSheet);
    Items.unregisterSheet("core", ItemSheet);

    // register custom sheets
    Actors.registerSheet("gurps", GurpsPlayerSheet, {
        types: ["player"],
        makeDefault: true,
        label: "Gurps.SheetPlayer"
    });

    Items.registerSheet("gurps", GurpsItemSheet, {
        types: ["base", "skill", "advantage", "equipment"],
        label: "Gurps.SheetItem"
    });

    // preloadHandlebarsTemplates();

    /**
     * Slugify a string.
     */
    Handlebars.registerHelper('slugify', function(value) {
        return value.slugify({strict: true});
    });

    Handlebars.registerHelper('numberFormat', function(value) {
        if (value >= 0) {
            return '+' + value;
        }
        return value;
    });

    Handlebars.registerHelper('JSON', function(value) {
        return JSON.stringify(value);
    });

    Handlebars.registerHelper('skillDiff', function(value) {
        return parseInt(value);
    });

    Handlebars.registerHelper('select', function(value, options) {
        var $el = $('<select />').html(options.fn(this));
        $el.find('[value="' + value + '"]').attr({selected: 'selected'});

        return $el.html();
    });

    loadTemplates([
        'systems/foundryvtt-gurps/templates/actors/partials/actor-characteristics.html',
        'systems/foundryvtt-gurps/templates/actors/partials/actor-equipment.html',
        'systems/foundryvtt-gurps/templates/actors/partials/actor-advantages.html',
        'systems/foundryvtt-gurps/templates/actors/partials/actor-skills.html',
        'systems/foundryvtt-gurps/templates/items/partials/item-type.html',
        'systems/foundryvtt-gurps/templates/partials/debug-entity.html',
    ]);
});