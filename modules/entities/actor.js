//reference: https://gitlab.com/foundrynet/dnd5e/~/blob/master/module/actor/entity.js
export default class GurpsActor extends Actor {
    /** @override */
    prepareBaseData() {
        switch(this.data.type) {
            case 'player':
                this._preparePlayerData(this.data);
                break;
            case 'npc':
                break;
        }
    }

    _calculateBasicSpeed(data) {
        return (data.DX.value + data.ST.value) / 4;
    }

    _calculateDodge(data) {
        return Math.floor(this._calculateBasicSpeed(data)) + 3;
    }

    /** @override */
    prepareDerivedData() {
        const data = this.data.data;

        // set any derived attributes

        // striking st page 88
        // lifting
        data.basic_speed = this._calculateBasicSpeed(data);
        data.dodge = this._calculateDodge(data);

        // damage.swing
        // damage.thrust
        // basic_lift
        // enumberance table

        this.items.forEach((item) => {
            // do stuff with items if needed
        });
    }

    _preparePlayerData(actor) {
        const data = actor.data;
        data.attr = 'an attribute';
    }
}